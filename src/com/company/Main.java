package com.company;

import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        System.out.println("Программа предназначенна для удаления из вашего текста слов которые имеют 5 букв и " +
                "начинаются на согласную.");
        System.out.println("Введите ваш текст: ");
        Scanner importText = new Scanner(System.in);
        String text = importText.nextLine();
        int lengthWord = 5;
        String consonants = "Й Ц К Н Г Ш Щ З Х Ъ Ф В П Р Л Д Ж Ч С М Т Ь Б й ц к н г ш щ з х ъ ф в п р л д ж ч с м т ь б";

        String[] literals = consonants.split("\\s");
        String[] words = text.split("\\s");

        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < literals.length; j++) {
                if (words[i].length() == lengthWord && words[i].startsWith(literals[j])) {
                    words[i] = "";
                }
            }
        }

        for (int i = 0; i < words.length; i++) {
            System.out.print(words[i] + " ");
        }
    }
}
